const url = "https://lobinhos.herokuapp.com/"
const $ = document
const id = sessionStorage.getItem("id")


arrumarLobo()

// Pegando o local dos valores:
const imgLoboLocal = $.querySelector("#img-lobo")
const tituloLocal = $.querySelector(".texto").children[0]
const idLocal = $.querySelector(".texto").children[1]


// Função para dar o get
function arrumarLobo() {
    fetch(url + "wolves/" + id)
    .then(wolf => wolf.json())
    .then(wolfJson => {
        const nome = wolfJson["name"]
        const urlFoto = wolfJson["image_url"]

        imgLoboLocal.style.backgroundImage = `url(${urlFoto})`
        tituloLocal.textContent = `Adote o(a) ${nome}`
        idLocal.textContent = `ID: ${id}`
    })
}


// btn-adotar
$.querySelector("#btn-adotar").addEventListener("click", () => {
    // Variável de validade e texto do alert
    let validade

    // Pegando os valores dos inputs
    const locNome = $.querySelector("#inp-nome")
    const nome = locNome.value

    const locIdade = $.querySelector("#inp-idade")
    const idade = locIdade.value

    const locEmail = $.querySelector("#inp-email")
    const email = locEmail.value

    // Checando as condições: 
    validade = validar(nome, idade, email)

    if (validade[0]) {
        locNome.value = ""
        locIdade.value = ""
        locEmail.value = ""

        adotarLobo(nome, idade, email)

        alert("Lobinho adotado com sucesso!!")
        
        window.location.href = `../paginas/listaLobinhos.html`
    } else {
        alert(validade[1])
    }
})


function validar(nome, idade, email) {
    let valid = true
    let invalidos = 0
    let alerta = ""


    if ((nome.replace(/\s/g,"") === "")
    || (nome.length < 4) 
    || (nome.length > 60)) {
        alerta += "Nome"
        valid = false
        invalidos++
    }

    if ((idade.replace(/\s/g,"") === "")
    || (idade < 0) 
    || (idade > 100)) {
        valid ? alerta += "Idade" : alerta += ", idade"
        valid = false
        invalidos++
    }

    if ((email.replace(/\s/g,"") === "")
    || (email.length < 10) 
    || (email.length > 225)) {
        valid ? alerta += "E-mail" : alerta += ", e-mail"
        valid = false
        invalidos++
    }

    invalidos > 1 ? alerta += " inválidos(as)" : alerta += " inválido(a)"

    return [valid, alerta]
}


function adotarLobo(nome, idade, email) {
    // Organizando o corpo:
    const body = {
        wolf: {
            adopter_name: nome,
            adotper_age: idade,
            adopter_email: email,
        }
    }


    // Organizando a config
    const config = {
        method: "PUT",
        body: JSON.stringify(body),
        headers: {
            "Content-Type": "application/json"
        }
    }

    // Enviando
    fetch(url + "wolves/" + id, config)
        .catch(error => console.log(error))
}