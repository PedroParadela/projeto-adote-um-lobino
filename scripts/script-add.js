const url = "https://lobinhos.herokuapp.com/"
const $ = document

// Pegando o botão
const btnSalvar = $.querySelector("#btn-salvar")


// Adicionando evento de clicar
btnSalvar.addEventListener("click", event => {
    
    // Variável de validade e texto do alert
    let validade

    // Pegando os valores dos inputs
    const locNome = $.querySelector("#inp-nome")
    const nome = locNome.value

    const locIdade = $.querySelector("#inp-anos")
    const idade = locIdade.value

    const locUrlfoto = $.querySelector("#inp-link")
    const urlfoto = locUrlfoto.value

    const locDesc = $.querySelector("#inp-desc")
    const desc = locDesc.value

    // Checando as condições: 
    validade = validar(nome, idade, urlfoto, desc)

    if (validade[0]) {
        locNome.value = ""
        locIdade.value = ""
        locUrlfoto.value = ""
        locDesc.value = ""

        cadastrarLobinho(nome, idade, urlfoto, desc)

        alert("Lobinho cadastado com sucesso!")
    } else {
        alert(validade[1])
    }
})


function validar(nome, idade, urlfoto, desc) {
    let valid = true
    let invalidos = 0
    let alerta = ""


    if ((nome.replace(/\s/g,"") === "")
    || (nome.length < 4) 
    || (nome.length > 60)) {
        alerta += "Nome"
        valid = false
        invalidos++
    }

    if ((idade.replace(/\s/g,"") === "")
    || (idade < 0) 
    || (idade > 100)) {
        valid ? alerta += "Idade" : alerta += ", idade"
        valid = false
        invalidos++
    }

    if ((urlfoto.replace(/\s/g,"") === "")) {
        valid ? alerta += "Url" : alerta += ", url"
        valid = false
        invalidos++
    }

    if ((desc.replace(/\s/g,"") === "")
    || (desc.length < 10) 
    || (desc.length > 225)) {
        valid ? alerta += "Descrição" : alerta += ", descrição"
        valid = false
        invalidos++
    }

    invalidos > 1 ? alerta += " inválidos(as)" : alerta += " inválido(a)"

    return [valid, alerta]
}


function cadastrarLobinho(nome, idade, urlfoto, desc) {
    // Organizando o corpo:
    const body = {
        wolf: {
            name: nome,
            age: idade,
            image_url: urlfoto,
            description: desc
        }
    }


    // Organizando a config
    const config = {
        method: "POST",
        body: JSON.stringify(body),
        headers: {
            "Content-Type": "application/json"
        }
    }

    // Enviando
    fetch(url + "/wolves", config)
        .catch(error => console.log(error))
}

