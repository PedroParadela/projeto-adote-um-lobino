const url = "https://lobinhos.herokuapp.com/"
const $ = document
const ulLobos = $.querySelector("#exemplos")

const quantidade = 10
let verAdotados = false
let filter = false
let filtered = []
let pagina = $.querySelector(".active")

let totalLobos = 0

arrumarLobinhos(quantidade, pagina.textContent, filter)


// Função que arruma os lobos (usa a variavel verAdotados)
function arrumarLobinhos(quantidade, pagina, filter, filtered=[]) {
    const path = verAdotados ? "wolves/adopted" : "wolves";
    fetch(url + path)
    .then(wolves => wolves.json())
    .then(wolvesJson => {
        if (!filter) {
            filter = wolvesJson
        }
        posicionarLobos(filter, pagina, quantidade)
    })
}


function posicionarLobos(wolvesJson, pagina, quantidade) {
    totalLobos = wolvesJson.length
    
        for (let i = 0 + (quantidade * (pagina - 1)); i < quantidade + (quantidade * (pagina - 1)) ; i++) {
            // Separando o Lobo
            lobo = wolvesJson[i]
            
            if (i === wolvesJson.length) {
                break
            }
            
            
            
            // Pegando as informações
            const id = lobo["id"]
            const nome = lobo["name"]
            const idade = lobo["age"]
            const desc = lobo["description"]
            const urlImg = lobo["image_url"]
            
            
            let umOuDois = i % 2 === 0 ? "um" : "dois"
            
            const li = $.createElement("li")
            li.classList.add("exemplo")
            li.classList.add(umOuDois)

            li.innerHTML = `
            <div class="img"></div>
            
            <div class="azul-relative">
                <div class="azul"></div>
            </div>
            
            
            <div class="exemplo-txt">
                <div class="titulo-btn">
                    <div>
                        <h4>${nome}</h4>
                        <h6>Idade: ${idade} anos</h6>
                        <h5 class="id none">${id}</h5>
                    </div>
                    


                </div>
                    <p>${desc}</p>
            </div>`
            
            const classImg = li.getElementsByClassName("img")[0]
            classImg.style.backgroundImage = `url(${urlImg})`

            
            classImg.addEventListener("click", () => {
                sessionStorage.setItem("id", id);
                sessionStorage.setItem("adotado", verAdotados)
                
                window.location.href = `../paginas/showlobinho.html`
                
            })
            
            
            li.getElementsByClassName("titulo-btn")[0].appendChild(verAdotados ? createBtnAdotado() : createBtnAdotar())

            ulLobos.appendChild(li)
        }
}

    
// Botão add lobo
$.querySelector("#btn-add").addEventListener("click", () => window.location.href = "../paginas/addlobinho.html")


// Checkbox
$.querySelector("#check").addEventListener("click", () => {
    verAdotados = !verAdotados
    const pai = ulLobos.parentElement

    resetPage()
})


// Paginação
const pagValues = $.querySelectorAll(".pag")


for (valor of pagValues) {
    valor.addEventListener("click", (event) => {
        let valorTxt = event.target.textContent

        switch (valorTxt) {
            case "«":
                if (pagina.textContent != "1") {

                    passarPagina(parseInt(pagina.textContent) - 1)
                    
                }
                break
            case "»":

                passarPagina(parseInt(pagina.textContent) + 1)
                    
                break

            case "=>":
                passarPagina(parseInt(totalLobos/quantidade) + 1)
                
                break
            default:
                passarPagina(valorTxt)
        }
    })
}


function passarPagina(pag) {
    if (pag < 8) {
        $.querySelector(".active").classList.remove("active")

        for (let i = 1; i < 8; i++) {
            $.querySelector(".pagination").children[i].getElementsByClassName("pag")[0].textContent = i
        }
        
        // try {
        // } catch (e) { }

        pagina.classList.remove("active")
        pagina = $.querySelector(".pagination").children[pag].getElementsByClassName("pag")[0]
        pagina.classList.add("active")
    } else {
        pagina.classList.remove("active")
        pagina = $.querySelector(".pagination").children[7].getElementsByClassName("pag")[0]
        $.querySelector(".pagination").children[6].getElementsByClassName("pag")[0].textContent = "..."
        pagina.textContent = pag
        pagina.classList.add("active")
    }
    resetPage()
}


function resetPage() {
    
    ulLobos.innerHTML = ""
    
    if (filter) {
        arrumarLobinhos(quantidade, pagina.textContent, filtered)
    } else {
        arrumarLobinhos(quantidade, pagina.textContent)
    }
}




function createBtnAdotar() {
    const btnAdotar = $.createElement("button")
    btnAdotar.classList.add("btn-adotar")
    btnAdotar.textContent = "Adotar"

    btnAdotar.addEventListener("click", () => {
        let id = btnAdotar.parentElement.getElementsByTagName("h5")[0].textContent

        sessionStorage.setItem("id", id);
        sessionStorage.setItem("adotado", false)
        window.location.href = `../paginas/adotarlobinho.html`
    })

    return btnAdotar;
    
}


function createBtnAdotado() {
    const btnAdotado = $.createElement("button")
    btnAdotado.classList.add("btn-adotado")
    btnAdotado.textContent = "Adotado"

    btnAdotado.addEventListener("click", () => {
        let id = btnAdotado.parentElement.getElementsByTagName("h5")[0].textContent

        sessionStorage.setItem("id", id)
        sessionStorage.setItem("adotado", true)
        window.location.href = `../paginas/showlobinho.html`

    })

    return btnAdotado;
    
}



// Pesquisa
$.querySelector("#lupa").addEventListener("click", () => executarPesquisa())
$.querySelector("#inp-texto").addEventListener("keypress", (event) => {
    if (event.keyCode === 13) {
        executarPesquisa()
    }
})


function executarPesquisa() {
    const inpPesquisa = $.querySelector("#inp-texto")
    const pesquisa = inpPesquisa.value.toLowerCase()

    if (pesquisa.replace(/\s/g,"") === "") {
        alert("Pesquisa inválida.")
    } else {
        const path = verAdotados ? "wolves/adopted" : "wolves";
        fetch(url + path)
        .then(response => response.json())
        .then(wolves => {
            filtered = wolves.filter(wolf => wolf["name"].toLowerCase().includes(pesquisa))
            filter = true
            console.log(filtered)
            resetPage(filtered)
        })
    }

}

function teste(txt) {
    return txt["name"].toLowerCase().includes("mi")
}

    const path = verAdotados ? "wolves/adopted" : "wolves";
