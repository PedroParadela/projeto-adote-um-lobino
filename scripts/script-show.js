let id = sessionStorage.getItem("id")
let adotado = sessionStorage.getItem("adotado")
const url = "https://lobinhos.herokuapp.com/"
const $ = document

const locNome = $.querySelector("#nome")
const locImagem = $.querySelector(".img")
const locDesc = $.querySelector("#desc")

const btnAdotar = $.querySelector("#btn-adotar")
const btnExcluir = $.querySelector("#btn-excluir")

console.log(adotado)
if (adotado == "true") {
    btnAdotar.classList.add("hidden")
}

iniciar()

function iniciar() {
    
    fetch(url + "wolves/" + id)
    .then(wolf => wolf.json())
    .then(wolfJson => {
        locNome.textContent = wolfJson["name"]
        locImagem.style.backgroundImage = `url(${wolfJson["image_url"]})`
        locDesc.textContent = wolfJson["description"]
    })
}


btnAdotar.addEventListener("click", () => {
    sessionStorage.setItem("id", id)
    window.location.href = "../paginas/adotarlobinho.html"
})

btnExcluir.addEventListener("click", () => {

    // Deletando mensagem especifica
    const config = {
        method: "DELETE"
    }

    fetch(url + "wolves/" + id, config)
        .catch(error => alert(error))
    
    
    alert("Excluído com sucesso.")
    window.location.href = "../paginas/listaLobinhos.html"
})
