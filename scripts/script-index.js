const url = "https://lobinhos.herokuapp.com/"
const $ = document


arrumarLobos()

// Função para pegar os valores e adicionar nos locais certos
function arrumarLobos() {
    for(let i = 1; i < 3; i++) {

        fetch(url + "wolves")
            .then(lobo => lobo.json())
            .then(loboJson => {
                // Pegando o local dos valores
                const fotoLobo = $.querySelector(`#lobo-${i}`)
                const nome = $.querySelector(`#exemplo-${i}`).getElementsByTagName("h4")[0]
                const idade = $.querySelector(`#exemplo-${i}`).getElementsByTagName("h6")[0]
                const descricao = $.querySelector(`#exemplo-${i}`).getElementsByTagName("p")[0]
                
                
                // Mudando o url do ref pelo css
                fotoLobo.style.cssText = `
                background-image: url(${loboJson[i]["image_url"]});
                background-position: center; 
                background-repeat: no-repeat; 
                background-size: cover;
                `

                // Pegando e botando o nome
                nome.innerText = `${loboJson[i]["name"]}`

                
                // Pegando e botando a idade
                idade.innerText = `Idade: ${loboJson[i]["age"]} anos.`

                
                // Pegando e botando a descrição
                descricao.innerText = `${loboJson[i]["description"]}`
            })
    }
}

